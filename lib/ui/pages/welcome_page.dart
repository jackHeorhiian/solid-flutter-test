import 'dart:math';

import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  ColorGenerator _colorGenerator;

  @override
  void initState() {
    super.initState();
    _colorGenerator = ColorGenerator(currentColor: Colors.white);
  }

  _setRandomBackgroundColor() {
    setState(() {
      _colorGenerator.generateRandomColor();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _setRandomBackgroundColor(),
      child: Scaffold(
        backgroundColor: _colorGenerator.currentColor,
        body: SafeArea(
          child: Center(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 8),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(33, 56, 75, 1),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Text(
                'Hey there',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ColorGenerator {
  Color currentColor;

  ColorGenerator({@required this.currentColor});

  Color generateRandomColor() => this.currentColor = Color.fromRGBO(
      _getRandomNumber(),
      _getRandomNumber(),
      _getRandomNumber(),
      _getRandomOpacity());

  int _getRandomNumber() => Random().nextInt(255);

  double _getRandomOpacity() => Random().nextDouble();
}
